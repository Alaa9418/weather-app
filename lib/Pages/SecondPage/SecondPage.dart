import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/Model/Forecast.dart';
import 'package:weather_app/ScopedModel/MainModel.dart';
import 'package:weather_app/Widgets/SecondPage/UIElements/ForecastCard.dart';

class SecondPage extends StatefulWidget {
  final int index;

  SecondPage({@required this.index});

  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  Widget _buildCityWidget() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Align(
          alignment: Alignment.topCenter,
          child: Text(
            model.weather.cityName,
            style: TextStyle(
              color: Colors.white,
              fontSize: 32,
            ),
          ),
        );
      },
    );
  }

  Widget _buildDescriptionWidget() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Align(
          alignment: Alignment.center,
          child: Text(
            model.generalForecastList[widget.index].weather[0].description,
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
        );
      },
    );
  }

  Widget _buildTempWidget() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Align(
          alignment: Alignment.topCenter,
          child: Text(
            '${model.generalForecastList[widget.index].main.temp.toStringAsPrecision(2)}°',
            style: TextStyle(
              color: Colors.white,
              fontSize: 51.2,
            ),
          ),
        );
      },
    );
  }

  Widget _buildDayTempMinMaxWidget() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Align(
                child: Text(
                  DateFormat('EEEE')
                      .format(model.generalForecastList[widget.index].dtTxt),
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                alignment: Alignment.centerLeft,
              ),
            ),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 20),
                  child: Align(
                    child: Text(
                      model.generalForecastList[widget.index].main.tempMax
                          .toStringAsPrecision(2),
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 8),
                  padding: EdgeInsets.only(left: 20),
                  child: Align(
                    child: Text(
                      model.generalForecastList[widget.index].main.tempMin
                          .toStringAsPrecision(2),
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  Widget _build3HoursForecastList() {
    double screenHeight = MediaQuery.of(context).size.height;
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        List<ListElement> forecast3HoursList = model.getForecastListEach3Hours(
            model.forecastList
                .indexOf(model.generalForecastList[widget.index]));
        return Container(
          height: 0.14 * screenHeight,
          child: ListView.builder(
            itemBuilder: (context, index) {
              print('Second page index: $index');
              print(
                  'Second page forecast3HoursList length ${forecast3HoursList.length}');
              print(
                  'Second page forecast#HoursList items: \n ${forecast3HoursList[index].dtTxt}');
              print(
                  'Second page forecast#HoursList items: \n ${forecast3HoursList[index].weather[0].icon}');
              print(
                  'Second page forecast#HoursList items: \n ${forecast3HoursList[index].main.temp}');
              return ForecastCard(index: index);
            },
            scrollDirection: Axis.horizontal,
            itemCount: forecast3HoursList.length,
          ),
        );
      },
    );
  }

  Widget _buildWindAndHumidity() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Align(
                  child: Text(
                    'WIND',
                    style: TextStyle(
                      color: Colors.grey[300],
                    ),
                  ),
                  alignment: Alignment.topLeft,
                ),
                Align(
                  child: Text(
                    '${model.generalForecastList[widget.index].wind.speed} KM',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                  alignment: Alignment.topLeft,
                ),
              ],
            ),
            SizedBox(
              width: 32,
            ),
            Column(
              children: [
                Align(
                  child: Text(
                    'HUMIDITY',
                    style: TextStyle(
                      color: Colors.grey[300],
                    ),
                  ),
                  alignment: Alignment.topLeft,
                ),
                Align(
                  child: Text(
                    '${model.generalForecastList[widget.index].main.humidity}',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                  alignment: Alignment.topLeft,
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  Widget _buildSeaLevelAndFeelsLike(){
    return ScopedModelDescendant<MainModel>(builder: (context, child, MainModel model) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              Align(
                child: Text(
                  'SEA LEVEL',
                  style: TextStyle(
                    color: Colors.grey[300],
                  ),
                ),
                alignment: Alignment.topLeft,
              ),
              Align(
                child: Text(
                  '${model.generalForecastList[widget.index].main.seaLevel}',
                  style: TextStyle(color: Colors.white, fontSize: 24),
                ),
                alignment: Alignment.topLeft,
              ),
            ],
          ),
          SizedBox(
            width: 32,
          ),
          Column(
            children: [
              Align(
                child: Text(
                  'FEELS LIKE',
                  style: TextStyle(
                    color: Colors.grey[300],
                  ),
                ),
                alignment: Alignment.topLeft,
              ),
              Align(
                child: Text(
                  '${model.generalForecastList[widget.index].main.feelsLike}°',
                  style: TextStyle(color: Colors.white, fontSize: 24),
                ),
                alignment: Alignment.topLeft,
              ),
            ],
          ),
        ],
      );
    },);
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    // print(widget.index);

    return ScopedModelDescendant<MainModel>(
      builder: (context, child, model) {
        // print(
        //     'general forecast list${model.generalForecastList[widget.index].dtTxt}');
        // print('next day index: ${model.forecastList.indexOf(model.generalForecastList[widget.index])}');
        // List<ListElement> forecast3HoursList = model.getForecastListEach3Hours(
        //     model.forecastList
        //         .indexOf(model.generalForecastList[widget.index]));
        // print('forecast list: ${model.getForecastListEach3Hours(widget.index)}');
        return Scaffold(
          backgroundColor: Colors.blue,
          body: ListView(
            children: [
              SizedBox(
                height: 0.1 * screenHeight,
              ),
              _buildCityWidget(),
              _buildDescriptionWidget(),
              SizedBox(
                height: 8,
              ),
              _buildTempWidget(),
              SizedBox(
                height: 64,
              ),
              _buildDayTempMinMaxWidget(),
              Divider(
                height: 1,
              ),
              SizedBox(
                height: 8,
              ),
              _build3HoursForecastList(),
              // Container(
              //   height: 0.14 * screenHeight,
              //   child: ListView.builder(
              //     itemBuilder: (context, index) {
              //       print('Second page index: $index');
              //       print(
              //           'Second page forecast3HoursList length ${forecast3HoursList.length}');
              //       print(
              //           'Second page forecast#HoursList items: \n ${forecast3HoursList[index].dtTxt}');
              //       print(
              //           'Second page forecast#HoursList items: \n ${forecast3HoursList[index].weather[0].icon}');
              //       print(
              //           'Second page forecast#HoursList items: \n ${forecast3HoursList[index].main.temp}');
              //       return ForecastCard(index: index);
              //     },
              //     scrollDirection: Axis.horizontal,
              //     itemCount: forecast3HoursList.length,
              //   ),
              // ),
              SizedBox(
                height: 8,
              ),
              Divider(
                height: 1,
              ),
              SizedBox(
                height: 8,
              ),
              _buildWindAndHumidity(),
              Divider(
                height: 8,
                endIndent: 32,
                indent: 32,
              ),
              _buildSeaLevelAndFeelsLike(),
            ],
          ),
        );
      },
    );
  }
}
