import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/ScopedModel/MainModel.dart';
import 'package:weather_app/Widgets/FirstPage/UIElements/OtherDayInfoCard.dart';

class FirstPage extends StatefulWidget {
  final MainModel model;

  FirstPage({this.model});

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  String currentDate = intl.DateFormat('MMM/dd').format(DateTime.now());

  @override
  void initState() {
    super.initState();
    widget.model.fetchCurrentWeather();
    widget.model.fetchForecastList();
  }

  Widget _buildCityWidget(MainModel model) {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Align(
          alignment: Alignment.center,
          child: Text(model.weather.cityName,
              style: TextStyle(
                  fontSize: 32,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
        );
      },
    );
  }

  Widget _buildDateTimeWidget() {
    return Align(
      alignment: Alignment.center,
      child: Text(
          '${intl.DateFormat('EEEE h:mm a, MMM dd').format(DateTime.now())}',
          style: TextStyle(fontSize: 16, color: Colors.white)),
    );
  }

  Widget _buildTempAndIcon() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('${model.weather.temp.toStringAsPrecision(2)}°',
                style: TextStyle(fontSize: 51.2, color: Colors.white)),
            Image.network(
              'http://openweathermap.org/img/w/' + model.weather.icon + '.png',
              fit: BoxFit.contain,
              width: 64,
              height: 64,
            ),
          ],
        );
      },
    );
  }

  Widget _buildDescription() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Align(
          alignment: Alignment.center,
          child: Text(model.weather.description,
              style: TextStyle(fontSize: 20, color: Colors.white)),
        );
      },
    );
  }

  Widget _buildInfoHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 8, bottom: 8),
      child: Align(
        alignment: Alignment.centerLeft,
        child:
            Text('Info', style: TextStyle(fontSize: 20, color: Colors.white)),
      ),
    );
  }

  Widget _buildInfoCard() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white.withOpacity(0.07),
          ),
          width: MediaQuery.of(context).size.width,
          height: 275,
          child: Column(
            children: [
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Align(
                    child: Text(
                      'SUNRISE',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  SizedBox(
                    width: 48,
                  ),
                  Align(
                    child: Text(
                      intl.DateFormat('jm').format(
                          DateTime.fromMillisecondsSinceEpoch(
                                  model.weather.sunrise * 1000,
                                  isUtc: true)
                              .add(const Duration(hours: 4))),
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    alignment: Alignment.bottomRight,
                  ),
                ],
              ),
              Divider(
                height: 24,
                indent: 24,
                endIndent: 24,
                color: Colors.white54,
                thickness: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Align(
                    child: Text(
                      'SUNSET',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  SizedBox(
                    width: 48,
                  ),
                  Align(
                    child: Text(
                      intl.DateFormat('jm').format(
                          DateTime.fromMillisecondsSinceEpoch(
                                  model.weather.sunset * 1000,
                                  isUtc: true)
                              .add(const Duration(hours: 4))),
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    alignment: Alignment.bottomRight,
                  ),
                ],
              ),
              Divider(
                height: 24,
                indent: 24,
                endIndent: 24,
                color: Colors.white54,
                thickness: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Align(
                    child: Text(
                      'Humidity',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  SizedBox(
                    width: 48,
                  ),
                  Align(
                    child: Text(
                      model.weather.humidity.toStringAsFixed(
                          model.weather.humidity.truncateToDouble() ==
                                  model.weather.humidity
                              ? 0
                              : 1),
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    alignment: Alignment.bottomRight,
                  ),
                ],
              ),
              Divider(
                height: 24,
                indent: 24,
                endIndent: 24,
                color: Colors.white54,
                thickness: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Align(
                    child: Text(
                      'Pressure',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  SizedBox(
                    width: 48,
                  ),
                  Align(
                    child: Text(
                      model.weather.pressure.toStringAsFixed(
                          model.weather.pressure.truncateToDouble() ==
                                  model.weather.pressure
                              ? 0
                              : 1),
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    alignment: Alignment.bottomRight,
                  ),
                ],
              ),
              Divider(
                height: 24,
                indent: 24,
                endIndent: 24,
                color: Colors.white54,
                thickness: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Align(
                    child: Text(
                      'Wind',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  SizedBox(
                    width: 48,
                  ),
                  Align(
                    child: Text(
                      model.weather.wind.toStringAsFixed(
                          model.weather.wind.truncateToDouble() ==
                                  model.weather.wind
                              ? 0
                              : 1),
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    alignment: Alignment.bottomRight,
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildForecastHeaderWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 8, bottom: 8),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text('Forecast',
            style: TextStyle(fontSize: 20, color: Colors.white)),
      ),
    );
  }

  Widget _buildGeneralForecastList() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Container(
          child: ListView.builder(
            itemCount: model.generalForecastList.length,
            itemBuilder: (context, index) => OtherDayInfoCard(
              index: index,
            ),
            scrollDirection: Axis.horizontal,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white.withOpacity(0.07),
          ),
          width: MediaQuery.of(context).size.width,
          height: 200,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScopedModelDescendant<MainModel>(
        builder: (context, child, MainModel model) {
          return RefreshIndicator(
            onRefresh: () {
              model.fetchCurrentWeather();
              model.fetchForecastList();
              return model.fetchCurrentWeather();
              // model.fetchForecastList();
            },
            child: Container(
              color: Colors.blue,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              // color: Theme.of(context).primaryColor,
              child: model.weather == null
                  ? Column(
                      children: [
                        SizedBox(
                          height: 250,
                        ),
                        Center(child: Text('No Internet Connection!!!')),
                        Center(
                          child: IconButton(
                            icon: Icon(Icons.refresh),
                            onPressed: () {
                              setState(() {
                                widget.model.fetchCurrentWeather();
                                widget.model.fetchForecastList();
                              });
                            },
                          ),
                        ),
                      ],
                    )
                  : ListView(
                      children: [
                        SizedBox(
                          height: 48,
                        ),
                        _buildCityWidget(model),
                        SizedBox(
                          height: 8,
                        ),
                        _buildDateTimeWidget(),
                        SizedBox(
                          height: 8,
                        ),
                        _buildTempAndIcon(),
                        SizedBox(
                          height: 8,
                        ),
                        _buildDescription(),
                        SizedBox(
                          height: 32,
                        ),
                        _buildInfoHeader(),
                        _buildInfoCard(),
                        SizedBox(
                          height: 32,
                        ),
                        _buildForecastHeaderWidget(),
                        _buildGeneralForecastList(),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }
}
