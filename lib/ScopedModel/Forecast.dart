import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/Model/Forecast.dart';
import 'package:http/http.dart' as http;

mixin ForecastModel on Model {
  List<ListElement> _forecastList = [];
  List<ListElement> _forecastForEach3HoursList = [];
  bool _isLoading = false;

  List<ListElement> get forecastList {
    return _forecastList;
  }

  List<ListElement> get generalForecastList {
    List<ListElement> generalList = [];
    for (int i = 0; i < _forecastList.length; i++) {
      if (i == 0) continue;
      // print(_forecastList[i].dtTxt);
      if (_forecastList[i].dtTxt.hour == 0) {
        generalList.add(_forecastList[i]);
      }
    }
    return generalList;
  }

  List<ListElement> get forecastForEach3HoursList{
    return _forecastForEach3HoursList;
  }

  List<ListElement> getForecastListEach3Hours(int index) {
    _forecastForEach3HoursList =[];
    if (_forecastList.length < index + 8) {
      print('if index: $index');
      for (int i = index; i < _forecastList.length; i++) {
        _forecastForEach3HoursList.add(_forecastList[i]);
      }
    } else {
      print('else index: $index');
      for (int i = index; i < index + 8; i++) {
        print('else i= $i');
        _forecastForEach3HoursList.add(_forecastList[i]);
        // print(_forecastList.indexOf(_forecastForEach3HoursList[i]));
        print('creating forecast list: ${_forecastList[i].dtTxt}');
        // print('creating forecast list: ${_forecastForEach3HoursList[i].dtTxt}');
      }
    }
    for(int i = 0; i < _forecastForEach3HoursList.length ; i++){
      print('forecast index = ${_forecastList.indexOf(_forecastForEach3HoursList[i])}');
    }
    return _forecastForEach3HoursList;
  }

  Future<bool> fetchForecastList() async {
    _isLoading = true;
    notifyListeners();
    final http.Response response = await http.get(
        'https://api.openweathermap.org/data/2.5/forecast?id=292223&units=metric&appid=ab2c0be97cf00d36bfbb19ac2d32e62f');

    if (response.statusCode != 200 && response.statusCode != 201) {
      print('Error with connection!!');
      return false;
    }
    var data = json.decode(response.body);
    if (data == null) {
      _isLoading = false;
      notifyListeners();
      return false;
    }
    Welcome welcome = Welcome.fromJson(data);
    print('Welcome Parsing data:');
    _forecastList = welcome.list;
    // print(DateTime.fromMillisecondsSinceEpoch(welcome.city.sunset));
    // final time =DateTime.fromMillisecondsSinceEpoch(welcome.city.sunset * 1000,
    //     isUtc: true)
    //     .add(const Duration(hours: 4));
    // print('test{$time}');
    // print(welcome.city.sunset);
    // print(DateTime.fromMillisecondsSinceEpoch(welcome.city.sunrise));
    // final time2 = DateTime.fromMillisecondsSinceEpoch(welcome.city.sunrise * 1000,
    //     isUtc: true)
    //     .add(const Duration(hours: 4));
    // print('test{$time2}');

    // List<ListElement> testList = getForecastListEach3Hours(_forecastList.indexOf(generalForecastList[4]));
    // print(testList.length);
    // for(var i = 0; i<testList.length;i++){
    //   print(testList[i].dtTxt);
    // }
    _isLoading = false;
    notifyListeners();
    return true;
  }
}
