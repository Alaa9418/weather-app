import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/Model/Weather.dart';
import 'package:http/http.dart' as http;

mixin WeatherModel on Model {
  Weather _weather;
  bool _isLoading = false;

  bool get isLoading {
    return _isLoading;
  }

  Weather get weather {
    return _weather;
  }

  Future<Null> fetchCurrentWeather() {
    _isLoading = true;
    notifyListeners();
    return http
        .get(
            'https://api.openweathermap.org/data/2.5/weather?q=Dubai&units=metric&appid=ab2c0be97cf00d36bfbb19ac2d32e62f')
        .then((http.Response response) {
      final Map<String, dynamic> weatherData = json.decode(response.body);
      if (weatherData == null) {
        _isLoading = false;
        print('Alaa:');
        notifyListeners();
        return;
      }
      _weather = Weather(
          cityName: weatherData['name'],
          description: weatherData['weather'][0]['description'],
          icon: weatherData['weather'][0]['icon'],
          temp: (weatherData['main']['temp']).toDouble(),
          sunrise: weatherData['sys']['sunrise'],
          sunset: weatherData['sys']['sunset'],
          humidity: weatherData['main']['humidity'].toDouble(),
          pressure: weatherData['main']['pressure'].toDouble(),
          wind: weatherData['wind']['speed'].toDouble());
      _isLoading = false;
      print('Alaa:');
      print(_weather);
      notifyListeners();
    });
  }
}
