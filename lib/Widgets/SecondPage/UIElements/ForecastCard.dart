import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/Model/Forecast.dart';
import 'package:weather_app/ScopedModel/MainModel.dart';

class ForecastCard extends StatelessWidget {
  final int index;

  ForecastCard({@required this.index});

  // List<Widget> _buildForecastCard(List<ListElement> forecast3HourseList) {
  //   List<Widget> widgetsList = [];
    // for (var item in forecast3HoursList) {
    //   widgetsList.add(
    //     Column(
    //       children: [
    //         Text(
    //           DateFormat('jm')
    //               .format(item.dtTxt),
    //           style: TextStyle(fontSize: 16, color: Colors.white),
    //         ),
    //         SizedBox(
    //           height: 4,
    //         ),
    //         Container(
    //           width: 40,
    //           height: 40,
    //           child: Image.network('http://openweathermap.org/img/w/' +
    //               item.weather[0].icon +
    //               '.png'),
    //         ),
    //         SizedBox(
    //           height: 4,
    //         ),
    //         Text(
    //           '39°',
    //           style: TextStyle(fontSize: 16, color: Colors.white),
    //         ),
    //       ],
    //     ),
    //   );
    // }
  // }

  @override
  Widget build(BuildContext context) {
    // print('card index: $index');
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Text(
                DateFormat('jm')
                    .format(model.forecastForEach3HoursList[index].dtTxt),
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              SizedBox(
                height: 4,
              ),
              Container(
                width: 40,
                height: 40,
                child: Image.network('http://openweathermap.org/img/w/' +
                    model.forecastForEach3HoursList[index].weather[0].icon +
                    '.png'),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                '${model.forecastForEach3HoursList[index].main.temp}°',
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ],
          ),
        );
      },
    );
  }
}
