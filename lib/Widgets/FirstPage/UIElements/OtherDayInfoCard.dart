import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:scoped_model/scoped_model.dart';
import 'package:weather_app/Pages/SecondPage/SecondPage.dart';
import 'package:weather_app/ScopedModel/MainModel.dart';

class OtherDayInfoCard extends StatelessWidget {
  final int index;

  OtherDayInfoCard({@required this.index});

  Widget _buildCardContent() {
    return ScopedModelDescendant<MainModel>(
      builder: (context, child, MainModel model) {
        return Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8),
              child: Align(
                child: Text(
                  intl.DateFormat('EEEE')
                      .format(model.generalForecastList[index].dtTxt),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                alignment: Alignment.topCenter,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              child: Image.network(
                'http://openweathermap.org/img/w/' +
                    model.generalForecastList[index].weather[0].icon +
                    '.png',
                fit: BoxFit.cover,
                width: 48,
                height: 48,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    '${model.generalForecastList[index].main.tempMax.toStringAsPrecision(2)}°',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    width: 3,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),color: Colors.white,
                        // border: Border(
                        //   left: BorderSide(color: Colors.white,width: 3)
                        // )
                      ),

                      height: 24),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    '${model.generalForecastList[index].main.tempMin.toStringAsPrecision(2)}°',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return SecondPage(
              index: index,
            );
          },
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),),
        // color: Colors.white,
        width: 100,
        // height: 50,
        margin: EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 8),
        child: _buildCardContent(),
      ),
    );
  }
}
