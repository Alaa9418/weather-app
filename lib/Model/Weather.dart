import 'package:flutter/cupertino.dart';

class Weather {
  final String cityName;
  final double temp;
  final String description;
  final String icon;
  final int sunrise;
  final int sunset;
  final double humidity;
  final double pressure;
  final double wind;

  Weather(
      {@required this.cityName,
      @required this.temp,
      @required this.description,
      @required this.icon,
      @required this.sunrise,
      @required this.sunset,
      @required this.humidity,
      @required this.pressure,
      @required this.wind});

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
        cityName: json['name'],
        temp: json['main']['temp'],
        description: json['weather'][0]['description'],
        icon: json['weather'][0]['icon'],
        sunrise: json['sys']['sunrise'],
        sunset: json['sys']['sunset'],
        humidity: json['main']['humidity'] as double,
        pressure: json['main']['pressure'] as double,
        wind: json['wind']['speed'] as double);
  }
}
